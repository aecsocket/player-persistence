# PlayerPersistence

Provides API for player bodies, manages corpses on player death, and allows persistent interaction
with logged out players in the world through fake entities, such as damaging and moving.

---

This plugin provides:
* Death corpses: spawn on player death and hold the player's items
* Player bodies on logout: interactable entities which represent the player's state e.g. health and inventory

## Setup

### Dependencies

* [Java >=16](https://adoptopenjdk.net/?variant=openjdk16&jvmVariant=hotspot)
* [Paper >=1.16.5](https://papermc.io/)
* [ProtocolLib >=4.6.0 Dev](https://ci.dmulloy2.net/job/ProtocolLib/lastSuccessfulBuild/)

### [Download](https://gitlab.com/aecsocket/player-persistence/-/jobs/artifacts/master/raw/target/PlayerPersistence.jar?job=build)

### Coordinates

Repository
```xml
<repository>
    <id>gitlab-maven-player-persistence</id>
    <url>https://gitlab.com/api/v4/projects/27032703/packages/maven</url>
</repository>
```

Dependency
```xml
<dependency>
    <groupId>com.gitlab.aecsocket</groupId>
    <artifactId>player-persistence</artifactId>
    <version>[VERSION]</version>
</dependency>
```

### API

Both main features - Persistence and DeathCorpses - interact with the Bodies API.
You can read the Javadocs to get usage information. Javadocs not hosted yet.
