package com.gitlab.aecsocket.playerpersistence.death;

import com.gitlab.aecsocket.minecommons.core.Logging;
import com.gitlab.aecsocket.minecommons.core.scheduler.Task;
import com.gitlab.aecsocket.playerpersistence.PlayerPersistencePlugin;
import com.gitlab.aecsocket.playerpersistence.body.Body;
import com.gitlab.aecsocket.playerpersistence.body.BodyOptions;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.spongepowered.configurate.objectmapping.ConfigSerializable;
import org.spongepowered.configurate.objectmapping.meta.Required;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Controls the creation of {@link Body} instances on player death.
 */
public final class DeathCorpses implements Listener {
    @ConfigSerializable
    public record Config(
            @Required boolean enabled,
            @Required long spawnDelay,
            @Required boolean hideNametags,
            @Required BodyOptions bodyOptions,
            @Required boolean storeInventory,
            @Required boolean storeExperience,
            @Required boolean onlyAccessibleToOwner
    ) {
        public static final Config DEFAULT = new Config(
                true, 1000,
                true, new BodyOptions(true, true, false),
                true, true, false);
    }

    private final PlayerPersistencePlugin plugin;
    private Config config;

    public DeathCorpses(PlayerPersistencePlugin plugin) {
        this.plugin = plugin;
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }

    public PlayerPersistencePlugin plugin() { return plugin; }
    public Config config() { return config; }

    public void load() {
        config = plugin.setting(Config.DEFAULT, (n, d) -> n.get(Config.class, d), "death_corpses");
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void event(PlayerDeathEvent event) {
        if (!config.enabled()) return;
        Player player = event.getEntity();
        Body body = Body.from(player);

        ItemStack[] contents = player.getInventory().getContents();

        AtomicBoolean storeInventory = new AtomicBoolean();
        AtomicBoolean storeExperience = new AtomicBoolean();
        if (!event.getKeepInventory()) {
            if (config.storeInventory) {
                storeInventory.set(true);
                event.getItemsToKeep().clear();
                event.getDrops().clear();
            }

            if (config.storeExperience) {
                storeExperience.set(true);
                event.setNewTotalExp(0);
                event.setShouldDropExperience(false);
            }
        }

        plugin.scheduler().run(Task.single(ctx -> {
            plugin.bodies().spawn(body, player.getLocation(), config.hideNametags, true, config.bodyOptions, instance -> {
                if (storeInventory.get())
                    instance.inventory(instance.inventoryFromPlayer(plugin, contents,
                            plugin.localize(plugin.defaultLocale(), "death_corpses.name",
                                    "name", player.displayName()), player.getInventory().getHeldItemSlot(), e -> {}));
                if (storeExperience.get())
                    instance.inventory().experience(event.getDroppedExp());
                if (config.onlyAccessibleToOwner)
                    instance.inventory().accessibleTo(player.getUniqueId());

                plugin.log(Logging.Level.DEBUG, "Player %s (%s) died, created body %s",
                        player.getName(), player.getUniqueId(), instance.entity().getUniqueId());
            });
        }, config.spawnDelay));
    }
}
