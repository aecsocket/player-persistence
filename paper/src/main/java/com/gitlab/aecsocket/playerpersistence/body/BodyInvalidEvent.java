package com.gitlab.aecsocket.playerpersistence.body;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

public class BodyInvalidEvent extends Event {
    private static final HandlerList handlers = new HandlerList();

    private final LivingEntity entity;

    public BodyInvalidEvent(LivingEntity entity) {
        this.entity = entity;
    }

    public LivingEntity entity() { return entity; }

    @Override @NotNull public HandlerList getHandlers() {
        return handlers;
    }
    @NotNull public static HandlerList getHandlerList() {
        return handlers;
    }
}
