package com.gitlab.aecsocket.playerpersistence.persistence;

import com.gitlab.aecsocket.playerpersistence.utils.SlotType;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.serialize.SerializationException;
import org.spongepowered.configurate.serialize.TypeSerializer;

import java.lang.reflect.Type;
import java.util.List;

public final class StateInventory {
    public static final int SIZE = (9 * 4) + 4 + 1; // = 41

    public static final class Serializer implements TypeSerializer<StateInventory> {
        @Override
        public void serialize(Type type, @Nullable StateInventory obj, ConfigurationNode node) throws SerializationException {
            if (obj == null) node.set(null);
            else {
                node.node("held-slot").set(obj.heldSlot);
                ConfigurationNode contents = node.node("contents");
                for (ItemStack slot : obj.contents) {
                    contents.appendListNode().set(slot == null ? new ItemStack(Material.AIR) : slot);
                }
            }
        }

        @Override
        public StateInventory deserialize(Type type, ConfigurationNode node) throws SerializationException {
            StateInventory result = new StateInventory();
            result.heldSlot = node.node("held-slot").getInt(0);
            List<? extends ConfigurationNode> children = node.node("contents").childrenList();
            for (int i = 0; i < Math.min(SIZE, children.size()); i++) {
                result.set(i, children.get(i).get(ItemStack.class));
            }
            return result;
        }
    }

    private ItemStack[] contents;
    private int heldSlot;

    public StateInventory() {
        contents = new ItemStack[SIZE];
    }

    public ItemStack[] contents() { return contents; }
    public void contents(ItemStack[] contents) {
        this.contents = new ItemStack[SIZE];
        System.arraycopy(contents, 0, this.contents, 0, Math.min(contents.length, this.contents.length));
    }

    public int heldSlot() { return heldSlot; }
    public StateInventory heldSlot(int heldSlot) { this.heldSlot = heldSlot; return this; }

    public ItemStack get(int slot) { return contents[slot]; }
    public ItemStack get(EquipmentSlot slot) { return get(SlotType.byEquipment(slot).id(heldSlot)); }
    public void set(int slot, ItemStack item) { contents[slot] = item; }

    public StateInventory updateFrom(Player player) {
        PlayerInventory inventory = player.getInventory();
        contents(inventory.getContents());
        heldSlot = inventory.getHeldItemSlot();
        return this;
    }

    public StateInventory applyTo(Player player) {
        PlayerInventory inventory = player.getInventory();
        inventory.setContents(contents);
        inventory.setHeldItemSlot(heldSlot);
        return this;
    }
}
