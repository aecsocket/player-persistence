package com.gitlab.aecsocket.playerpersistence.body;

import com.comphenix.protocol.wrappers.WrappedGameProfile;
import com.destroystokyo.paper.ClientOption;
import com.gitlab.aecsocket.minecommons.core.scheduler.Task;
import com.gitlab.aecsocket.minecommons.paper.EventInventoryHolder;
import com.gitlab.aecsocket.minecommons.paper.PaperUtils;
import com.gitlab.aecsocket.playerpersistence.PlayerPersistencePlugin;
import com.gitlab.aecsocket.playerpersistence.utils.SlotType;
import net.kyori.adventure.text.Component;
import org.bukkit.Bukkit;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryEvent;
import org.bukkit.event.inventory.InventoryInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;
import org.spongepowered.configurate.objectmapping.ConfigSerializable;

import java.util.UUID;
import java.util.function.Consumer;

@ConfigSerializable
public record Body(
        UUID id,
        Component name,
        byte skinParts,
        WrappedGameProfile baseProfile
) {
    public final class Instance {
        public class BodyHolder implements EventInventoryHolder {
            private final PlayerPersistencePlugin plugin;
            private final Consumer<InventoryEvent> inventoryCallback;

            public BodyHolder(PlayerPersistencePlugin plugin, Consumer<InventoryEvent> inventoryCallback) {
                this.plugin = plugin;
                this.inventoryCallback = inventoryCallback;
            }

            public BodyHolder(PlayerPersistencePlugin plugin) {
                this(plugin, e -> {});
            }

            @Override public PlayerPersistencePlugin plugin() { return plugin; }

            @Override public @NotNull Inventory getInventory() { return Instance.this.inventory().inventory(); }

            private boolean invalid(int topSlots, int slot, ItemStack item) {
                if (slot < topSlots) {
                    if (slot > SlotType.HIGHEST_SLOT)
                        return true;
                    if (PaperUtils.empty(item))
                        return false;
                    SlotType type = SlotType.BY_ID.get(slot);
                    if (type == null)
                        return false;
                    return !type.test().test(item);
                }
                return false;
            }

            @Override
            public void event(InventoryEvent event) {
                if (event instanceof InventoryClickEvent click) {
                    int topSlots = click.getView().getTopInventory().getSize();
                    if (invalid(topSlots, click.getRawSlot(), click.getCursor())) {
                        click.setCancelled(true);
                        return;
                    }
                }
                if (event instanceof InventoryDragEvent drag) {
                    int topSlots = drag.getView().getTopInventory().getSize();
                    for (var entry : drag.getNewItems().entrySet()) {
                        if (invalid(topSlots, entry.getKey(), entry.getValue())) {
                            drag.setCancelled(true);
                            return;
                        }
                    }
                }

                if (event instanceof InventoryInteractEvent interact) {
                    plugin.scheduler().run(Task.single(ctx -> {
                        plugin.bodies().updateEquipment(Instance.this, plugin.protocol().packetTargets((Player) interact.getWhoClicked()));
                        inventoryCallback.accept(event);
                    }, 0));
                }
            }
        }

        private final long creationTime;
        private final WrappedGameProfile profile;
        private final LivingEntity entity;
        private final boolean hideNametag;
        private final boolean expires;
        private BodyInventory inventory;
        private BodyOptions options;
        private Consumer<Body.Instance> removeCallback;

        public Instance(WrappedGameProfile profile, LivingEntity entity, boolean hideNametag, boolean expires, BodyInventory inventory, BodyOptions options, Consumer<Body.Instance> removeCallback) {
            creationTime = System.currentTimeMillis();
            this.profile = profile;
            this.entity = entity;
            this.hideNametag = hideNametag;
            this.expires = expires;
            this.inventory = inventory;
            this.options = options;
            this.removeCallback = removeCallback;
        }

        public Body body() { return Body.this; }
        public long creationTime() { return creationTime; }
        public WrappedGameProfile profile() { return profile; }
        public LivingEntity entity() { return entity; }
        public boolean hideNametag() { return hideNametag; }
        public boolean expires() { return expires; }

        public BodyInventory inventory() { return inventory; }
        public Instance inventory(BodyInventory inventory) { this.inventory = inventory; return this; }

        public BodyOptions options() { return options; }
        public Instance options(BodyOptions options) { this.options = options; return this; }

        public Consumer<Body.Instance> removeCallback() { return removeCallback; }
        public Instance removeCallback(Consumer<Body.Instance> onRemove) { this.removeCallback = onRemove; return this; }

        public BodyInventory inventoryFromPlayer(PlayerPersistencePlugin plugin, ItemStack[] contents, Component name, int heldSlot, Consumer<InventoryEvent> inventoryCallback) {
            Inventory inventory = Bukkit.createInventory(new BodyHolder(plugin, inventoryCallback), BodyInventory.PLAYER_INVENTORY_SIZE, name);
            inventory.setContents(contents);
            return new BodyInventory(
                    inventory, slot -> {
                        int id = SlotType.byEquipment(slot).id();
                        return id >= 0 ? inventory.getItem(id) : inventory.getItem(heldSlot);
                    }
            );
        }
    }

    public static Body from(Player player) {
        return new Body(player.getUniqueId(), player.displayName(),
                (byte) player.getClientOption(ClientOption.SKIN_PARTS).getRaw(),
                WrappedGameProfile.fromPlayer(player));
    }

    public Instance instance(WrappedGameProfile profile, LivingEntity entity, boolean hideNametag, boolean expires, BodyOptions options) {
        return new Instance(profile, entity, hideNametag, expires, new BodyInventory(), options, null);
    }

}
