package com.gitlab.aecsocket.playerpersistence.persistence;

import com.gitlab.aecsocket.minecommons.core.Logging;
import com.gitlab.aecsocket.playerpersistence.PlayerPersistencePlugin;
import com.gitlab.aecsocket.playerpersistence.body.Body;
import com.gitlab.aecsocket.playerpersistence.body.BodyInvalidEvent;
import com.gitlab.aecsocket.playerpersistence.body.BodyOptions;
import org.bukkit.*;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.persistence.PersistentDataType;
import org.spongepowered.configurate.ConfigurateException;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.loader.ConfigurationLoader;
import org.spongepowered.configurate.objectmapping.ConfigSerializable;
import org.spongepowered.configurate.objectmapping.meta.Required;

import java.util.*;
import java.util.function.Function;

public final class Persistence implements Listener {
    public static final String KEY_DEFAULT = "default";
    public static final String PATH_DATA = "persistence.conf";

    @ConfigSerializable
    public record Config(
            @Required Map<String, WorldConfig> worlds
    ) {
        public static final Config DEFAULT = new Config(Collections.emptyMap());

        public WorldConfig world(String world) { return worlds.getOrDefault(world, worlds.getOrDefault(KEY_DEFAULT, WorldConfig.DEFAULT)); }
        public WorldConfig world(World world) { return world(world.getName()); }
    }

    @ConfigSerializable
    public record WorldConfig(
            @Required String group,
            @Required boolean enabled,
            boolean resetStateOnMoveTo,
            EnumSet<GameMode> ignoreGamemodes,
            boolean hideNametags,
            BodyOptions bodyOptions
    ) {
        public static final WorldConfig DEFAULT = new WorldConfig(KEY_DEFAULT, true, true, EnumSet.of(GameMode.CREATIVE, GameMode.SPECTATOR),
                false, new BodyOptions(false, false, true));

        public boolean shouldSpawnCarrier(Player player) {
            return !ignoreGamemodes.contains(player.getGameMode());
        }
    }

    private final PlayerPersistencePlugin plugin;
    private Config config;
    private boolean virtual = true;
    private final Map<String, Set<String>> groups = new HashMap<>();
    // all loaded states
    private final Map<String, Map<UUID, PlayerState>> states = new HashMap<>();
    // chunks which we have made sure have no unloaded carriers
    private final Map<UUID, Set<Long>> loadedChunks = new HashMap<>();

    public Persistence(PlayerPersistencePlugin plugin) {
        this.plugin = plugin;
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }

    public void onDisable() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            safePersist(player.getLocation(), player, false);
        }
    }

    public PlayerPersistencePlugin plugin() { return plugin; }
    public Config config() { return config; }
    public Map<String, Set<String>> groups() { return groups; }
    public Map<String, Map<UUID, PlayerState>> states() { return states; }

    public void load() {
        config = plugin.setting(Config.DEFAULT, (n, d) -> n.get(Config.class, d), "persistence");
        groups.clear();
        for (var entry : config.worlds.entrySet()) {
            groups.computeIfAbsent(entry.getValue().group, g -> new HashSet<>())
                    .add(entry.getKey());
        }

        try {
            Map<Object, ? extends ConfigurationNode> nStates = plugin.loader(plugin.file(PATH_DATA))
                    .load().childrenMap();
            states.clear();
            for (var entry : nStates.entrySet()) {
                Map<UUID, PlayerState> add = new HashMap<>();
                for (var entry2 : entry.getValue().childrenMap().entrySet()) {
                    add.put(UUID.fromString(entry2.getKey().toString()), entry2.getValue().get(PlayerState.class));
                }
                states.put(entry.getKey().toString(), add);
            }

            virtual = false;
        } catch (ConfigurateException e) {
            plugin.log(Logging.Level.WARNING, e, "Could not load persistence data from %s", PATH_DATA);
            virtual = true;
        }
    }

    public void save() {
        // if data wasn't loaded properly in the first place, don't save it
        if (virtual)
            return;

        try {
            ConfigurationLoader<?> loader = plugin.loader(plugin.file(PATH_DATA));
            ConfigurationNode node = loader.createNode();
            for (var entry : states.entrySet()) {
                ConfigurationNode node2 = node.node(entry.getKey());
                for (var entry2 : entry.getValue().entrySet()) {
                    node2.node(entry2.getKey().toString()).set(entry2.getValue());
                }
            }
            plugin.loader(plugin.file(PATH_DATA)).save(node);
        } catch (ConfigurateException e) {
            plugin.log(Logging.Level.WARNING, e, "Could not save persistence data to %s", PATH_DATA);
        }
    }

    public String group(String world) { return config.world(world).group; }
    public String group(World world) { return group(world.getName()); }

    public PlayerState state(String group, UUID id) {
        return states.getOrDefault(group, Collections.emptyMap()).get(id);
    }
    public PlayerState state(String group, UUID id, Function<UUID, PlayerState> supplier) {
        return states.computeIfAbsent(group, g -> new HashMap<>()).computeIfAbsent(id, supplier);
    }
    public void state(String group, UUID id, PlayerState state) {
        states.computeIfAbsent(group, g -> new HashMap<>()).put(id, state);
    }


    public PlayerState getCarrier(LivingEntity entity) {
        UUID id = toUUID(entity.getPersistentDataContainer().get(plugin.key("carrier"), PersistentDataType.LONG_ARRAY));
        if (id == null)
            return null;
        PlayerState state = state(group(entity.getWorld()), id);
        if (state == null) {
            entity.remove();
            return null;
        }
        return state;
    }

    public void setCarrier(LivingEntity entity, PlayerState state) {
        entity.getPersistentDataContainer().set(plugin.key("carrier"), PersistentDataType.LONG_ARRAY, toLongs(state.body().id()));
    }

    public void spawnCarrier(Location location, PlayerState state) {
        WorldConfig cfg = config.world(location.getWorld());
        // - make a body
        // - set up its inventory/remove callback
        // - mark it as a carrier
        Body.Instance inst = plugin.bodies().spawn(state.body(), location, cfg.hideNametags, false, cfg.bodyOptions, instance -> {
                    instance.inventory(instance.inventoryFromPlayer(
                            plugin, state.inventory().contents(),
                            plugin.localize(plugin.defaultLocale(), "persistence.name",
                                    "name", state.body().name()),
                            state.inventory().heldSlot(),
                            event -> state.inventory().contents(event.getInventory().getContents())
                    ));
                })
                .removeCallback(instance -> state.updateFrom(instance.entity()));
        setCarrier(inst.entity(), state);
    }

    public void removeCarrier(Body.Instance carrier, PlayerState state) {
        state.updateFrom(carrier.entity());
        carrier.entity().remove();
    }


    public void restore(World world, Player player, boolean teleport) {
        WorldConfig cfg = config.world(world);

        // remove old carrier
        // we do this first, so we put the carrier's state into memory, then load that state
        UUID removed = null;
        for (var body : new ArrayList<>(plugin.bodies().bodies().values())) { // copy list to avoid concurrent modification
            if (!body.entity().getWorld().equals(world))
                continue;
            PlayerState state = getCarrier(body.entity());
            if (state == null || !state.body().id().equals(player.getUniqueId()))
                continue;
            removed = body.entity().getUniqueId();
            removeCarrier(body, state);
            break;
        }

        PlayerState state = state(cfg.group, player.getUniqueId());
        if (state != null) {
            state.applyTo(player, teleport);
        }
        plugin.log(Logging.Level.DEBUG, "Restored %s (%s) in %s (%s), removed carrier? %s, had state? %s",
                player.getName(), player.getUniqueId(),
                world.getName(), cfg.group,
                removed != null, state != null);
    }

    public void safeRestore(World world, Player player, boolean teleport) {
        WorldConfig cfg = config.world(world);
        if (!cfg.enabled)
            return;
        restore(world, player, teleport);
    }

    public void persist(Location location, Player player, boolean spawnCarrier) {
        WorldConfig cfg = config.world(location.getWorld());
        PlayerState state = state(cfg.group, player.getUniqueId(), id -> new PlayerState(player))
                .updateFrom(player);
        spawnCarrier = spawnCarrier && cfg.shouldSpawnCarrier(player);
        if (spawnCarrier) {
            spawnCarrier(location, state);
        }
        plugin.log(Logging.Level.DEBUG, "Persisted %s (%s) in %s (%s), spawned carrier? %s",
                player.getName(), player.getUniqueId(),
                location.getWorld().getName(), cfg.group,
                spawnCarrier);
    }
    public void persist(Location location, Player player) { persist(location, player, true); }

    public void safePersist(Location location, Player player, boolean spawnCarrier) {
        WorldConfig cfg = config.world(location.getWorld());
        if (!cfg.enabled)
            return;
        persist(location, player, spawnCarrier);
    }
    public void safePersist(Location location, Player player) { safePersist(location, player, true); }


    @EventHandler
    private void event(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        safeRestore(player.getWorld(), player, true);
    }

    @EventHandler
    private void event(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        safePersist(player.getLocation(), player);
    }

    @EventHandler
    private void event(PlayerTeleportEvent event) {
        Player player = event.getPlayer();
        Location from = event.getFrom();
        WorldConfig fromCfg = config.world(from.getWorld());
        Location to = event.getTo();
        WorldConfig toCfg = config.world(to.getWorld());
        if (!fromCfg.group.equals(toCfg.group)) {
            // moved from one world group to another
            // persist in the old, restore in the new
            plugin.log(Logging.Level.DEBUG, "Player %s (%s) teleported from %s (%s) to %s (%s)",
                    player.getName(), player.getUniqueId(),
                    from.getWorld().getName(), fromCfg.group,
                    to.getWorld().getName(), toCfg.group);
            safePersist(from, player);
            if (toCfg.resetStateOnMoveTo) {
                PlayerState.defaultFrom(player).applyTo(player, false);
            }
            safeRestore(to.getWorld(), player, false);
        }
    }

    @EventHandler
    private void event(BodyInvalidEvent event) {
        LivingEntity entity = event.entity();
        PlayerState state = getCarrier(entity);
        if (state == null)
            return;
        UUID id = state.body().id();
        String group = group(entity.getWorld());
        if (Bukkit.getPlayer(id) != null)
            return;
        for (var existing : plugin.bodies().bodies().values()) {
            if (!group(existing.entity().getWorld()).equals(group))
                continue;
            // a body for this state already exists
            if (existing.body().id().equals(id))
                return;
        }
        // there is no existing body, so we spawn one
        spawnCarrier(entity.getLocation(), state);
    }

    public static long[] toLongs(UUID uuid) { return new long[] { uuid.getMostSignificantBits(), uuid.getLeastSignificantBits() }; }
    public static UUID toUUID(long[] longs) { return longs == null ? null : new UUID(longs[0], longs[1]); }
}
