package com.gitlab.aecsocket.playerpersistence.body;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.ListeningWhitelist;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.events.PacketListener;
import com.comphenix.protocol.utility.MinecraftReflection;
import com.comphenix.protocol.wrappers.*;
import com.destroystokyo.paper.event.entity.EntityRemoveFromWorldEvent;
import com.gitlab.aecsocket.minecommons.core.CollectionBuilder;
import com.gitlab.aecsocket.minecommons.core.scheduler.Task;
import com.gitlab.aecsocket.minecommons.paper.PaperUtils;
import com.gitlab.aecsocket.minecommons.paper.plugin.ProtocolConstants;
import com.gitlab.aecsocket.minecommons.paper.plugin.ProtocolLibAPI;
import com.gitlab.aecsocket.playerpersistence.PlayerPersistencePlugin;
import org.apache.commons.lang.RandomStringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.plugin.Plugin;
import org.spongepowered.configurate.objectmapping.ConfigSerializable;
import org.spongepowered.configurate.objectmapping.meta.Required;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

import static com.comphenix.protocol.PacketType.Play.Server.*;

public final class Bodies implements Listener, PacketListener {
    private static final String base = "ppbodies";
    private static final WrappedChatComponent empty = WrappedChatComponent.fromText("");
    private static final String optionNever = "never";
    private enum ChatFormat {
        BLACK, DARK_BLUE, DARK_GREEN, DARK_AQUA, DARK_RED, DARK_PURPLE, GOLD, GRAY,
        DARK_GRAY, BLUE, GREEN, AQUA, RED, LIGHT_PURPLE, YELLOW, WHITE,
        OBFUSCATED, BOLD, STRIKETHROUGH, UNDERLINE, ITALIC, RESET
    }
    private static final Class<?> chatFormatClass = MinecraftReflection.getMinecraftClass("EnumChatFormat");
    private static final int removeInfoDelay = 1500;
    private static final ListeningWhitelist sendingWhitelist = ListeningWhitelist.newBuilder()
            .types(SPAWN_ENTITY_LIVING, ENTITY_METADATA, ENTITY_HEAD_ROTATION, ENTITY_TELEPORT,
                    ENTITY_VELOCITY, ENTITY_LOOK, REL_ENTITY_MOVE, REL_ENTITY_MOVE_LOOK)
            .build();

    @ConfigSerializable
    public record Config(
            @Required long expireTime,
            @Required long emptyExpireTime,
            @Required double interactRadius,
            @Required double playerInteractRadius
    ) {
        public static final Config DEFAULT = new Config(300_000, 30_000, 1, 3);
    }

    private final PlayerPersistencePlugin plugin;
    private final ProtocolLibAPI protocol;
    private Config config;
    private final Map<UUID, Body.Instance> bodies = new HashMap<>();

    public Bodies(PlayerPersistencePlugin plugin) {
        this.plugin = plugin;
        protocol = plugin.protocol();
        Bukkit.getPluginManager().registerEvents(this, plugin);
        protocol.manager().addPacketListener(this);
    }

    public PlayerPersistencePlugin plugin() { return plugin; }
    public Config config() { return config; }
    public Map<UUID, Body.Instance> bodies() { return bodies; }

    public void load() {
        config = plugin.setting(Config.DEFAULT, (n, d) -> n.get(Config.class, d), "bodies");
    }

    public Body.Instance register(Body.Instance body) {
        bodies.put(body.entity().getUniqueId(), body);
        return body;
    }

    public Body.Instance body(LivingEntity entity) {
        Body.Instance body = bodies.get(entity.getUniqueId());
        if (
                (body != null && expired(body))
                || (body == null && entity.getPersistentDataContainer().has(plugin.key("body"), PersistentDataType.STRING))
        ) {
            new BodyInvalidEvent(entity).callEvent();
            entity.remove();
            return null;
        }
        return body;
    }

    public void body(LivingEntity entity, Body.Instance body) {
        bodies.put(entity.getUniqueId(), body);
    }

    public boolean expired(Body.Instance body) {
        return body.expires() && System.currentTimeMillis() > body.creationTime() +
                (body.inventory().empty() ? config.emptyExpireTime : config.expireTime);
    }

    public WrappedGameProfile copyOf(WrappedGameProfile profile, boolean hideNametag) {
        WrappedGameProfile result = new WrappedGameProfile(UUID.randomUUID(),
                hideNametag ? base + RandomStringUtils.random(8, true, true) : profile.getName());
        result.getProperties().putAll(profile.getProperties());
        return result;
    }

    public List<WrappedWatchableObject> metadata(Body.Instance body) {
        ProtocolLibAPI.WatcherObjectsBuilder builder = ProtocolLibAPI.watcherObjects()
                .add(16, // Skin parts
                        Byte.class,
                        body.body().skinParts())
                .add(6, // Entity pose
                        EnumWrappers.getEntityPoseClass(),
                        (body.options().sleeping() ? EnumWrappers.EntityPose.SLEEPING : EnumWrappers.EntityPose.STANDING).toNms());
        if (body.options().sleeping()) {
            Location location = body.entity().getLocation().clone();
            location.setY(0);
            builder
                .add(13, // Bed position
                        WrappedDataWatcher.Registry.getBlockPositionSerializer(true),
                        Optional.of(BlockPosition.getConverter().getGeneric(new BlockPosition(location.toVector()))));
        }
        return builder.get();
    }

    public void update(Body.Instance body, Iterable<Player> viewers) {
        body.options().apply(body.entity());
        int entityId = body.entity().getEntityId();
        var metaPacket = protocol.build(PacketType.Play.Server.ENTITY_METADATA, packet -> {
            packet.getIntegers().write(0, entityId);
            // rest filled in by intercepting the packet
        });
        var movePacket = protocol.build(PacketType.Play.Server.REL_ENTITY_MOVE, packet -> {
            packet.getIntegers().write(0, entityId);
            packet.getShorts().write(0, (short) 0);
            packet.getShorts().write(1, (short) 0);
            packet.getShorts().write(2, (short) 0);
            packet.getBytes().write(0, (byte) 0);
            packet.getBytes().write(1, (byte) 0);
            packet.getBooleans().write(0, true);
            packet.getBooleans().write(1, true);
            packet.getBooleans().write(2, true);
        });

        for (Player viewer : viewers) {
            protocol.send(viewer, metaPacket);
            protocol.send(viewer, movePacket);
        }
        updateEquipment(body, viewers);
    }

    public void updateEquipment(Body.Instance body, Iterable<Player> viewers) {
        var equipmentPacket = protocol.build(PacketType.Play.Server.ENTITY_EQUIPMENT, packet -> {
            packet.getIntegers().write(0, body.entity().getEntityId());
            List<Pair<EnumWrappers.ItemSlot, ItemStack>> equipment = new ArrayList<>();
            for (var entry : ProtocolConstants.SLOTS.entrySet()) {
                equipment.add(new Pair<>(entry.getValue(), body.inventory().equipment(entry.getKey())));
            }
            packet.getSlotStackPairLists().write(0, equipment);
        });

        for (Player viewer : viewers) {
            protocol.send(viewer, equipmentPacket);
        }
    }

    public void spawn(Body.Instance body, Iterable<Player> viewers) {
        int entityId = body.entity().getEntityId();
        Location location = body.entity().getLocation();
        WrappedChatComponent nameComponent = WrappedChatComponent.fromText(body.profile().getName());

        var protocol = plugin.protocol();
        var addInfoPacket = protocol.build(PacketType.Play.Server.PLAYER_INFO, packet -> {
            packet.getPlayerInfoAction().write(0, EnumWrappers.PlayerInfoAction.ADD_PLAYER);
            packet.getPlayerInfoDataLists().write(0, CollectionBuilder.list(new ArrayList<PlayerInfoData>())
                    .add(new PlayerInfoData(body.profile(), -1, EnumWrappers.NativeGameMode.NOT_SET, nameComponent))
                    .get()
            );
        });
        var playerSpawnPacket = protocol.build(PacketType.Play.Server.NAMED_ENTITY_SPAWN, packet -> {
            packet.getIntegers().write(0, entityId);
            packet.getUUIDs().write(0, body.profile().getUUID());
            packet.getDoubles().write(0, location.getX());
            packet.getDoubles().write(1, location.getY());
            packet.getDoubles().write(2, location.getZ());
            packet.getBytes().write(0, (byte) 0);
            packet.getBytes().write(1, (byte) 0);
        });
        var movePacket = protocol.build(PacketType.Play.Server.REL_ENTITY_MOVE, packet -> {
            packet.getIntegers().write(0, entityId);
            packet.getShorts().write(0, (short) 0);
            packet.getShorts().write(1, protocol.protocolDelta(0.1f));
            packet.getShorts().write(2, (short) 0);
            packet.getBytes().write(0, (byte) 0);
            packet.getBytes().write(1, (byte) 0);
            packet.getBooleans().write(0, true);
            packet.getBooleans().write(1, true);
            packet.getBooleans().write(2, true);
        });
        var teamPacket = body.hideNametag() ? protocol.build(PacketType.Play.Server.SCOREBOARD_TEAM, packet -> {
            packet.getStrings().write(0, base);
            packet.getChatComponents().write(0, empty);
            packet.getChatComponents().write(1, empty);
            packet.getChatComponents().write(2, empty);
            packet.getStrings().write(1, optionNever);
            packet.getStrings().write(2, optionNever);
            packet.getEnumModifier(ChatFormat.class, chatFormatClass).write(0, ChatFormat.RESET);
            packet.getSpecificModifier(Collection.class).write(0, Collections.singleton(body.profile().getName()));
            packet.getIntegers().write(0, 3);
        }) : null;
        var removeInfoPacket = protocol.build(PacketType.Play.Server.PLAYER_INFO, packet -> {
            packet.getPlayerInfoAction().write(0, EnumWrappers.PlayerInfoAction.REMOVE_PLAYER);
            packet.getPlayerInfoDataLists().write(0, CollectionBuilder.list(new ArrayList<PlayerInfoData>())
                    .add(new PlayerInfoData(body.profile(), -1, EnumWrappers.NativeGameMode.NOT_SET, nameComponent))
                    .get()
            );
        });

        Location bedLocation = new Location(location.getWorld(), location.getX(), 0, location.getZ());
        for (Player viewer : viewers) {
            viewer.sendBlockChange(bedLocation, Material.WHITE_BED.createBlockData());
            protocol.send(viewer, addInfoPacket);
            protocol.send(viewer, playerSpawnPacket);
            protocol.send(viewer, movePacket);
            if (teamPacket != null)
                protocol.send(viewer, teamPacket);
        }
        bedLocation.getBlock().getState().update();

        plugin.scheduler().run(Task.single(ctx -> {
            update(body, viewers);
        }, 0));
        plugin.scheduler().run(Task.single(ctx -> {
            for (Player viewer : viewers) {
                protocol.send(viewer, removeInfoPacket);
            }
        }, removeInfoDelay));
    }

    public Body.Instance instance(Body body, LivingEntity entity, boolean hideNametag, boolean expires, BodyOptions options) {
        return body.instance(copyOf(body.baseProfile(), hideNametag), entity, hideNametag, expires, options);
    }

    public Body.Instance spawn(Body body, Location location, boolean hideNametag, boolean expires, BodyOptions options, Consumer<Body.Instance> initializer) {
        AtomicReference<Body.Instance> result = new AtomicReference<>();
        location.getWorld().spawnEntity(location, EntityType.ZOMBIE, CreatureSpawnEvent.SpawnReason.CUSTOM, raw -> {
            Zombie zombie = (Zombie) raw;
            Bukkit.getMobGoals().removeAllGoals(zombie);
            zombie.customName(body.name());
            zombie.stopDrowning();
            zombie.setRemoveWhenFarAway(false);
            zombie.setShouldBurnInDay(false);
            zombie.setCanPickupItems(false);
            zombie.setSilent(true);
            EntityEquipment equipment = zombie.getEquipment();
            for (EquipmentSlot slot : EquipmentSlot.values()) {
                equipment.setItem(slot, null);
            }
            Body.Instance instance = instance(body, zombie, hideNametag, expires, options);
            result.set(instance);
            initializer.accept(instance);
            instance.options().apply(zombie);
            zombie.getPersistentDataContainer().set(plugin.key("body"), PersistentDataType.STRING, "");
            body(zombie, instance);
        });
        return result.get();
    }

    @EventHandler
    private void event(PlayerJoinEvent event) {
        protocol.send(event.getPlayer(), PacketType.Play.Server.SCOREBOARD_TEAM, packet -> {
            packet.getStrings().write(0, base);
            packet.getChatComponents().write(0, empty);
            packet.getChatComponents().write(1, empty);
            packet.getChatComponents().write(2, empty);
            packet.getStrings().write(1, optionNever);
            packet.getStrings().write(2, optionNever);
            packet.getEnumModifier(ChatFormat.class, chatFormatClass).write(0, ChatFormat.RESET);
            packet.getSpecificModifier(Collection.class).write(0, Collections.emptySet());
            packet.getIntegers().write(0, 0);
        });
    }

    @EventHandler
    private void event(EntityDamageEvent event) {
        if (!(event.getEntity() instanceof LivingEntity entity))
            return;
        Body.Instance body = body(entity);
        if (body == null)
            return;
        if (body.options().invulnerable())
            event.setCancelled(true);
    }

    @EventHandler
    private void event(EntityRemoveFromWorldEvent event) {
        Body.Instance body = bodies.remove(event.getEntity().getUniqueId());
        if (body != null) {
            if (body.inventory().inventory() != null) {
                for (HumanEntity viewer : new ArrayList<>(body.inventory().inventory().getViewers())) {
                    viewer.closeInventory();
                }
            }

            if (body.removeCallback() != null) {
                body.removeCallback().accept(body);
            }
        }
    }

    @EventHandler
    private void event(PlayerInteractEvent event) {
        Action action = event.getAction();
        if (
                event.getHand() == EquipmentSlot.OFF_HAND
                || (action != Action.RIGHT_CLICK_AIR && action != Action.RIGHT_CLICK_BLOCK)
        )
            return;
        Player player = event.getPlayer();
        Location target = PaperUtils.target(player);
        Location location = player.getLocation();
        for (LivingEntity entity : target.getNearbyEntitiesByType(LivingEntity.class, config.interactRadius)) {
            Body.Instance body = body(entity);
            if (body == null)
                continue;
            if (target.distance(entity.getLocation()) > config.interactRadius || target.distance(location) > config.playerInteractRadius)
                continue;
            if (new BodyOpenEvent(player, entity, body).callEvent()) {
                body.inventory().open(player);
            }
        }
    }

    @EventHandler
    private void event(PlayerInteractEntityEvent event) {
        if (!(event.getRightClicked() instanceof LivingEntity entity))
            return;
        Player player = event.getPlayer();
        Body.Instance body = body(entity);
        if (body == null)
            return;
        if (player.getLocation().distance(entity.getLocation()) > config.playerInteractRadius)
            return;
        if (new BodyOpenEvent(player, entity, body).callEvent()) {
            body.inventory().open(player);
        }
    }

    @Override public Plugin getPlugin() { return plugin; }
    @Override public ListeningWhitelist getReceivingWhitelist() { return ListeningWhitelist.EMPTY_WHITELIST; }
    @Override public void onPacketReceiving(PacketEvent e) {}

    @Override public ListeningWhitelist getSendingWhitelist() { return sendingWhitelist; }
    @Override
    public void onPacketSending(PacketEvent event) {
        Player player = event.getPlayer();
        PacketContainer packet = event.getPacket();
        PacketType type = event.getPacketType();

        if (type == PacketType.Play.Server.SPAWN_ENTITY_LIVING) {
            if (!(packet.getEntityModifier(event).read(0) instanceof LivingEntity entity))
                return;
            Body.Instance body = body(entity);
            if (body == null)
                return;

            event.setCancelled(true);
            spawn(body, Collections.singleton(player));
        }

        if (type == PacketType.Play.Server.ENTITY_METADATA) {
            if (!(packet.getEntityModifier(event).read(0) instanceof LivingEntity entity))
                return;
            Body.Instance body = body(entity);
            if (body == null)
                return;

            packet.getWatchableCollectionModifier().write(0, metadata(body));
        }

        /*if (type == PacketType.Play.Server.ENTITY_HEAD_ROTATION || type == PacketType.Play.Server.ENTITY_TELEPORT) {
            if (!(packet.getEntityModifier(event).read(0) instanceof LivingEntity entity))
                return;
            Body.Instance body = body(entity);
            if (body == null)
                return;
            event.setCancelled(true);
        }*/
    }
}
