package com.gitlab.aecsocket.playerpersistence;

import cloud.commandframework.ArgumentDescription;
import cloud.commandframework.arguments.flags.CommandFlag;
import cloud.commandframework.arguments.standard.*;
import cloud.commandframework.bukkit.parsers.PlayerArgument;
import cloud.commandframework.bukkit.parsers.WorldArgument;
import cloud.commandframework.bukkit.parsers.location.LocationArgument;
import cloud.commandframework.context.CommandContext;
import com.gitlab.aecsocket.minecommons.paper.plugin.BaseCommand;
import com.gitlab.aecsocket.playerpersistence.body.Bodies;
import com.gitlab.aecsocket.playerpersistence.body.Body;
import com.gitlab.aecsocket.playerpersistence.body.BodyInventory;
import com.gitlab.aecsocket.playerpersistence.body.BodyOptions;
import com.gitlab.aecsocket.playerpersistence.persistence.Persistence;
import com.gitlab.aecsocket.playerpersistence.persistence.PlayerState;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.util.Vector;

import java.util.*;
import java.util.function.BiFunction;

/* package */ class PlayerPersistenceCommand extends BaseCommand<PlayerPersistencePlugin> {
    enum InventoryMode {
        NONE, REFERENCE, COPY
    }

    public PlayerPersistenceCommand(PlayerPersistencePlugin plugin) throws Exception {
        super(plugin, "player-persistence",
                (mgr, root) -> mgr.commandBuilder(root, ArgumentDescription.of("Plugin main command."), "persist"));

        var bodiesRoot = root
                .literal("bodies", ArgumentDescription.of("Bodies utility commands."));
        var persistenceRoot = root
                .literal("persistence", ArgumentDescription.of("Persistence feature commands."));

        BiFunction<CommandContext<CommandSender>, String, List<String>> suggestGroups =
                (ctx, arg) -> new ArrayList<>(plugin.persistence().groups().keySet());

        manager.command(bodiesRoot
                .literal("create", ArgumentDescription.of("Creates a body of a player."))
                .argument(PlayerArgument.optional("target"), ArgumentDescription.of("The player to create a body of."))
                .argument(LocationArgument.optional("location"), ArgumentDescription.of("The location to spawn the body at."))
                .argument(EnumArgument.optional(InventoryMode.class, "inventory-mode"),
                        ArgumentDescription.of("How the body's inventory should be portrayed."))
                .flag(CommandFlag.newBuilder("show-nametag").withAliases("n").withDescription(ArgumentDescription.of("Shows body nametag.")))
                .flag(CommandFlag.newBuilder("no-expire").withAliases("e").withDescription(ArgumentDescription.of("Body will not expire.")))
                .flag(CommandFlag.newBuilder("sleeping").withAliases("s").withDescription(ArgumentDescription.of("Sleeping body, not standing.")))
                .flag(CommandFlag.newBuilder("invulnerable").withAliases("i").withDescription(ArgumentDescription.of("Body can not be damaged.")))
                .flag(CommandFlag.newBuilder("collidable").withAliases("c").withDescription(ArgumentDescription.of("Body can be moved by pushing.")))
                .permission("%s.command.bodies.create".formatted(rootName))
                .handler(c -> handle(c, this::bodiesCreate)));
        manager.command(bodiesRoot
                .literal("remove", ArgumentDescription.of("Removes all bodies in a radius."))
                .argument(DoubleArgument.of("radius"), ArgumentDescription.of("The radius to remove bodies in."))
                .argument(LocationArgument.optional("center"), ArgumentDescription.of("The center of the radius to remove bodies in."))
                .permission("%s.command.bodies.remove".formatted(rootName))
                .handler(c -> handle(c, this::bodiesRemove)));

        manager.command(persistenceRoot
                .literal("groups", ArgumentDescription.of("Lists world group information."))
                .argument(StringArgument.<CommandSender>newBuilder("group")
                        .greedy()
                        .withSuggestionsProvider(suggestGroups)
                        .asOptional().build(), ArgumentDescription.of("The group to list information for."))
                .permission("%s.command.persistence.groups".formatted(rootName))
                .handler(c -> handle(c, this::persistenceGroups)));
        manager.command(persistenceRoot
                .literal("state", ArgumentDescription.of("Gets the saved state of a player."))
                .argument(UUIDArgument.of("id"), ArgumentDescription.of("The UUID of the player."))
                .argument(WorldArgument.optional("world"), ArgumentDescription.of("The world to get the state for."))
                .permission("%s.command.persistence.state".formatted(rootName))
                .handler(c -> handle(c, this::persistenceState)));
    }

    private String format(Location location) {
        return "%.2f, %.2f, %.2f".formatted(location.getX(), location.getY(), location.getZ());
    }

    private String format(Vector position) {
        return "%.2f, %.2f, %.2f".formatted(position.getX(), position.getY(), position.getZ());
    }

    protected void bodiesCreate(CommandContext<CommandSender> ctx, CommandSender sender, Locale locale, Player pSender) {
        Player target = defaultedArg(ctx, "target", pSender, () -> pSender);
        Location location = ctx.getOrDefault("location", target.getLocation());
        InventoryMode inventoryMode = ctx.getOrDefault("inventory-mode", InventoryMode.NONE);

        Bodies bodies = plugin.bodies();
        Body body = Body.from(target);

        bodies.spawn(body, location, !ctx.flags().isPresent("show-nametag"), !ctx.flags().isPresent("no-expire"), new BodyOptions(
                ctx.flags().isPresent("sleeping"),
                ctx.flags().isPresent("invulnerable"),
                ctx.flags().isPresent("collidable")
        ), instance -> {
            PlayerInventory inv = target.getInventory();
            instance.inventory(switch (inventoryMode) {
                case REFERENCE -> new BodyInventory(target.getInventory(), target.getInventory()::getItem);
                case COPY -> instance.inventoryFromPlayer(plugin, inv.getContents(), target.displayName(), inv.getHeldItemSlot(), e -> {});
                case NONE -> new BodyInventory();
            });
        });

        target.sendMessage(localize(locale, "chat.bodies.create",
                "player", target.displayName(),
                "location", format(location)));
    }

    protected void bodiesRemove(CommandContext<CommandSender> ctx, CommandSender sender, Locale locale, Player pSender) {
        double radius = ctx.get("radius");
        Location center = defaultedArg(ctx, "center", pSender, pSender::getLocation);

        Bodies bodies = plugin.bodies();
        int removed = 0;
        for (Entity raw : center.getNearbyEntitiesByType(null, radius)) {
            if (!(raw instanceof LivingEntity entity))
                continue;
            if (bodies.body(entity) != null) {
                entity.remove();
                ++removed;
            }
        }

        sender.sendMessage(localize(locale, "chat.bodies.remove",
                "removed", Integer.toString(removed)));
    }

    protected void persistenceGroups(CommandContext<CommandSender> ctx, CommandSender sender, Locale locale, Player pSender) {
        Persistence persistence = plugin.persistence();
        Collection<String> groups = ctx.<String>getOptional("group")
                .map(Collections::singleton)
                .orElse(persistence.groups().keySet());

        for (String group : groups) {
            Set<String> worlds = persistence.groups().get(group);
            if (worlds == null) {
                sender.sendMessage(localize(locale, "chat.error.invalid_group",
                        "group", group));
                continue;
            }
            sender.sendMessage(localize(locale, "chat.persistence.groups",
                    "group", group,
                    "worlds", String.join(", ", worlds)));
        }
    }

    protected void persistenceState(CommandContext<CommandSender> ctx, CommandSender sender, Locale locale, Player pSender) {
        UUID id = ctx.get("id");
        World world = defaultedArg(ctx, "world", pSender, pSender::getWorld);

        Persistence persistence = plugin.persistence();
        String group = persistence.group(world);
        PlayerState state = plugin.persistence().state(group, id);
        if (state == null)
            throw error("no_state",
                    "id", id.toString(),
                    "group", group);

        sender.sendMessage(localize(locale, "chat.persistence.state",
                "name", state.body().name(),
                "id", id.toString(),
                "health", "%.2f".formatted(state.health()),
                "max_health", "%.2f".formatted(state.maxHealth()),
                "location", format(state.position())));
    }
}
