package com.gitlab.aecsocket.playerpersistence.persistence;

import com.gitlab.aecsocket.playerpersistence.body.Body;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;
import org.spongepowered.configurate.objectmapping.ConfigSerializable;

@ConfigSerializable
public final class PlayerState {
    private final Body body;
    private final StateInventory inventory;
    private double health;
    private double maxHealth;
    private Vector position;

    public PlayerState(Body body, StateInventory inventory) {
        this.body = body;
        this.inventory = inventory;
    }

    public PlayerState(Player player) {
        body = Body.from(player);
        inventory = new StateInventory();
    }

    private PlayerState() { this(null, null); }

    public static PlayerState defaultFrom(Player player) {
        double health = player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getBaseValue();
        return new PlayerState(player)
                .maxHealth(health)
                .health(health)
                .position(player.getLocation().toVector());
    }

    public static PlayerState from(Player player) { return new PlayerState(player).updateFrom(player); }

    public Body body() { return body; }
    public StateInventory inventory() { return inventory; }

    public double health() { return health; }
    public PlayerState health(double health) { this.health = health; return this; }

    public double maxHealth() { return maxHealth; }
    public PlayerState maxHealth(double maxHealth) { this.maxHealth = maxHealth; return this; }

    public Vector position() { return position; }
    public PlayerState position(Vector position) { this.position = position; return this; }

    public PlayerState updateFrom(LivingEntity entity) {
        health = entity.getHealth();
        position = entity.getLocation().toVector();
        return this;
    }

    public PlayerState updateFrom(Player player) {
        maxHealth = player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue();
        inventory.updateFrom(player);
        return updateFrom((LivingEntity) player);
    }

    public PlayerState applyTo(LivingEntity entity, boolean teleport) {
        entity.customName(body.name());
        entity.setHealth(health);
        entity.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(maxHealth);
        if (teleport)
            entity.teleport(position.toLocation(entity.getWorld()));
        return this;
    }
    public PlayerState applyTo(LivingEntity entity) { return applyTo(entity, true); }

    public PlayerState applyTo(Player player, boolean teleport) {
        inventory.applyTo(player);
        return applyTo((LivingEntity) player, teleport);
    }
    public PlayerState applyTo(Player player) { return applyTo(player, true); }
}
