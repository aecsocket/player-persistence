package com.gitlab.aecsocket.playerpersistence.utils;

import com.destroystokyo.paper.MaterialTags;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

import java.util.Comparator;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum SlotType {
    FEET        (36, EquipmentSlot.FEET, MaterialTags.BOOTS::isTagged),
    LEGS        (37, EquipmentSlot.LEGS, MaterialTags.LEGGINGS::isTagged),
    CHEST       (38, EquipmentSlot.CHEST, MaterialTags.CHEST_EQUIPPABLE::isTagged),
    HEAD        (39, EquipmentSlot.HEAD, MaterialTags.HEAD_EQUIPPABLE::isTagged),
    OFF_HAND    (40, EquipmentSlot.OFF_HAND, i -> true),
    HAND        (-1, EquipmentSlot.HAND, i -> true);

    public static final Map<Integer, SlotType> BY_ID = Stream.of(values())
            .collect(Collectors.toMap(t -> t.id, Function.identity()));
    public static final Map<EquipmentSlot, SlotType> BY_EQUIPMENT = Stream.of(values())
            .collect(Collectors.toMap(t -> t.equipment, Function.identity()));
    public static final int HIGHEST_SLOT = Stream.of(values())
            .min(Comparator.comparingInt(t -> -t.id))
            .orElse(HAND).id;

    private final int id;
    private final EquipmentSlot equipment;
    private final Predicate<ItemStack> test;

    SlotType(int id, EquipmentSlot equipment, Predicate<ItemStack> test) {
        this.id = id;
        this.equipment = equipment;
        this.test = test;
    }

    public static SlotType byId(int id) { return BY_ID.get(id); }
    public static SlotType byEquipment(EquipmentSlot equipment) { return BY_EQUIPMENT.get(equipment); }

    public int id() { return id; }
    public EquipmentSlot equipment() { return equipment; }
    public Predicate<ItemStack> test() { return test; }

    public int id(int heldSlot) { return id == -1 ? heldSlot : id; }
    public boolean test(ItemStack item) { return test.test(item); }
}
