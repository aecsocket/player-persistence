package com.gitlab.aecsocket.playerpersistence.body;

import org.bukkit.entity.Player;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;
import java.util.function.Function;

public final class BodyInventory {
    public static final int PLAYER_INVENTORY_SIZE = 9 * 5;

    private final Inventory inventory;
    private final Function<EquipmentSlot, ItemStack> equipment;
    private int experience;
    private UUID accessibleTo;

    public BodyInventory(Inventory inventory, Function<EquipmentSlot, ItemStack> equipment) {
        this.inventory = inventory;
        this.equipment = equipment;
    }

    public BodyInventory() {
        this(null, null);
    }

    public Inventory inventory() { return inventory; }
    public Function<EquipmentSlot, ItemStack> equipment() { return equipment; }

    public int experience() { return experience; }
    public BodyInventory experience(int experience) { this.experience = experience; return this; }

    public UUID accessibleTo() { return accessibleTo; }
    public BodyInventory accessibleTo(UUID accessibleTo) { this.accessibleTo = accessibleTo; return this; }

    public ItemStack equipment(EquipmentSlot slot) {
        return equipment == null ? null : equipment.apply(slot);
    }

    public boolean empty() {
        return (inventory == null || inventory.isEmpty()) && experience <= 0;
    }

    public void open(Player player) {
        if (accessibleTo != null && !accessibleTo.equals(player.getUniqueId()))
            return;

        if (inventory != null)
            player.openInventory(inventory);
        if (experience > 0) {
            player.giveExp(experience);
            experience = 0;
        }
    }
}
