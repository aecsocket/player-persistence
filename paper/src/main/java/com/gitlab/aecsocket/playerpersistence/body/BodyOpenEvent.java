package com.gitlab.aecsocket.playerpersistence.body;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

public class BodyOpenEvent extends Event implements Cancellable {
    private static final HandlerList handlers = new HandlerList();

    private final Player player;
    private final LivingEntity clicked;
    private final Body.Instance body;
    private boolean cancelled;

    public BodyOpenEvent(Player player, LivingEntity clicked, Body.Instance body) {
        this.player = player;
        this.clicked = clicked;
        this.body = body;
    }

    public Player player() { return player; }
    public LivingEntity clicked() { return clicked; }
    public Body.Instance body() { return body; }

    @Override public boolean isCancelled() { return cancelled; }
    @Override public void setCancelled(boolean cancelled) { this.cancelled = cancelled; }

    @Override @NotNull public HandlerList getHandlers() {
        return handlers;
    }
    @NotNull public static HandlerList getHandlerList() {
        return handlers;
    }
}
