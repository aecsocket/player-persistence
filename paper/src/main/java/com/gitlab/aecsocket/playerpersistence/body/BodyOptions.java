package com.gitlab.aecsocket.playerpersistence.body;

import org.bukkit.entity.LivingEntity;
import org.spongepowered.configurate.objectmapping.ConfigSerializable;
import org.spongepowered.configurate.objectmapping.meta.Required;

@ConfigSerializable
public record BodyOptions(
        @Required boolean sleeping,
        @Required boolean invulnerable,
        @Required boolean collidable
) {
    public void apply(LivingEntity entity) {
        entity.setInvulnerable(invulnerable);
        entity.setCollidable(collidable);
    }
}
