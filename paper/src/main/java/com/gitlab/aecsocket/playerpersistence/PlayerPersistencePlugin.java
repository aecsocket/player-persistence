package com.gitlab.aecsocket.playerpersistence;

import com.gitlab.aecsocket.minecommons.core.Logging;
import com.gitlab.aecsocket.minecommons.paper.EventInventoryHolder;
import com.gitlab.aecsocket.minecommons.paper.plugin.BaseCommand;
import com.gitlab.aecsocket.minecommons.paper.plugin.BasePlugin;
import com.gitlab.aecsocket.minecommons.paper.scheduler.PaperScheduler;
import com.gitlab.aecsocket.playerpersistence.body.Bodies;
import com.gitlab.aecsocket.playerpersistence.death.DeathCorpses;
import com.gitlab.aecsocket.playerpersistence.persistence.Persistence;
import com.gitlab.aecsocket.playerpersistence.persistence.StateInventory;
import org.bstats.bukkit.Metrics;
import org.bukkit.*;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.objectmapping.ObjectMapper;
import org.spongepowered.configurate.serialize.TypeSerializerCollection;

/**
 * PlayerPersistence's main plugin class.
 */
public class PlayerPersistencePlugin extends BasePlugin<PlayerPersistencePlugin> {
    public static final int BSTATS_ID = 11556;

    private final PaperScheduler scheduler = new PaperScheduler(this);
    private Bodies bodies;
    private DeathCorpses deathCorpses;
    private Persistence persistence;

    @Override
    public void onEnable() {
        super.onEnable();
        bodies = new Bodies(this);
        deathCorpses = new DeathCorpses(this);
        persistence = new Persistence(this);

        Bukkit.getPluginManager().registerEvents(new EventInventoryHolder.EventListener(this), this);
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, this::save, 0,
                (long) (setting(30d, ConfigurationNode::getDouble, "save_interval") * 1200));
    }

    @Override
    public void onDisable() {
        persistence.onDisable();
        save();
    }

    @Override
    protected void configOptionsDefaults(TypeSerializerCollection.Builder serializers, ObjectMapper.Factory.Builder mapperFactory) {
        super.configOptionsDefaults(serializers, mapperFactory);
        serializers.register(StateInventory.class, new StateInventory.Serializer());
    }

    public PaperScheduler scheduler() { return scheduler; }
    public Bodies bodies() { return bodies; }
    public DeathCorpses deathCorpses() { return deathCorpses; }
    public Persistence persistence() { return persistence; }

    @Override
    public void load() {
        if (setting(true, ConfigurationNode::getBoolean, "enable_bstats")) {
            Metrics metrics = new Metrics(this, BSTATS_ID);
        }

        bodies.load();
        deathCorpses.load();
        persistence.load();
    }

    /**
     * Saves all plugin and feature data.
     */
    public void save() {
        persistence.save();
        log(Logging.Level.VERBOSE, "Saved all data");
    }

    @Override
    public void reload() {
        save();
        super.reload();
    }

    @Override
    protected BaseCommand<PlayerPersistencePlugin> createCommand() throws Exception {
        return new PlayerPersistenceCommand(this);
    }
}
