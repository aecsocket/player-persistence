pluginManagement {
    repositories {
        gradlePluginPortal()
    }
}

rootProject.name = "player-persistence"

include("paper")
